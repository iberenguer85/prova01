<?php
require_once("libOrdenarArray/Db.php");
require_once("libOrdenarArray/Utils.php");
//namespace libOrdenarArray\Controller;
class Controller
{

    /**
     * @description Setejem les variables que anirem modificant durant la sessió
     * @return Db $obj
     */
    public static function init()
    {
        $obj = new Db();
        $obj->historic[date("Y-m-d H:i:s")] = "inici";
        return $obj;
    }

    /**
     * @description S'encarrega de executar una funció o una altra segons el paràmetre que l'hi han passat per CLI
     * @param Db $obj
     * @param $argv, Array de paràmetres ja setejat en l'index.php
     */
    public static function route(Db $obj, $argv)
    {
        switch ($argv[1]) {
            case '-h':
                self::index($obj);
                break;
            case '1': //fet r2 (omitits notices)
                $obj = self::ordenarPerContingut($obj);
                self::tornarIndex($obj);
                break;
            case '2': //fet r2
                $obj = self::ordenarPerTipus($obj, $argv);
                self::tornarIndex($obj);
                break;
            case '3': //fet r2, no filtro els fills ja que en tipus array els fills son string i queden buits.
                $obj = self::filtrarPerTipus($obj);
                self::tornarIndex($obj);
                break;
            case '4': //fet r2
                $obj = self::reordenarRandom($obj);
                self::tornarIndex($obj);
                break;
            case '5': //fet r2
                $obj = self::resetArrayOriginal($obj);
                self::tornarIndex($obj);
            case '6': //no acabat
                $obj = self::eliminarElements3Chars($obj);
                self::tornarIndex($obj);
            case '7': //fet r2
                self::imprimirActual($obj);
                self::tornarIndex($obj);
                break;
            case '8': //fet r2
                self::imprimirOrigen($obj);
                self::tornarIndex($obj);
                break;
            case '9': //fet r2
                self::imprimirHistoric($obj);
                self::tornarIndex($obj);
                break;
            default: //fet r2
                if ($argv[1] != "10") {
                    echo "\n\nL'opció '" . $argv[1] . "' no té cap funcionalitat programada.";
                    self::tornarIndex($obj);
                } else {
                    echo "\n\nAdéu.";
                    exit;
                }
        }
    }

    /**
     * @description Mostra les opcions diponibles per introduir
     */
    public static function index(Db $obj)
    {
        echo "\n Menú d'opcions:
        \n\t'1'\t: Ordenar (+inversament)
            
        \n\t'2'\t: Ordenar per tipus (+inversament)

        \n\t'3'\t: Filtrar per tipus (+inversament)

        \n\t'4'\t: Reordenar aleatòriament

        \n\t'5'\t: Resetejar el contingut a l'array original

        \n\t'6'\t: Eliminar tots els elements que no continguin més de 3 caràcters entre 'n' i 't' (minúscules o majúscules)
        
        \n\t'7'\t: Imprimir l'array actual
        \n\t'8'\t: Imprimir l'array origen
        \n\t'9'\t: Imprimir l'historial de modificacions
        \n\t'10'\t: Sortir
        ";
        echo "\nTria una opció [1-10]:";
        $resposta = Utils::llegirLinia();
        $opcio = array();
        if ($resposta > 0 && $resposta < 11) {
            $opcio[1] = $resposta;
            self::route($obj, $opcio);
        } else {
            echo "\nOpció incorrecte.";
            self::tornarIndex($obj);
        }
    }

    /**
     * @description Demana si es vol tornar al menú o sortir del programa
     * @param Db $obj
     * @return void
     */
    public static function tornarIndex(Db $obj)
    {
        echo "\nVols tornar al menú? (1=si, 2=no)\n";
        $retorn = Utils::llegirLinia();
        $check = Utils::checkTipus($retorn, [1, 2]);
        if ($check) {
            if ($retorn == "1") {
                self::index($obj);
            } else {
                echo "Sortint del programa... A reveure.";
                exit;
            }
        } else {
            echo "\nLa opció '$retorn' no està dins els paràmetres vàlids: [1,2] ";
            self::tornarIndex($obj);
        }
    }


    /**
     * @description Opció 1. Ordenar per contingut
     * @param Db $obj
     * @return Db
     */
    public static function ordenarPerContingut(Db $obj)
    {
        $resp = Utils::detectarTipus($obj);
        if (is_array($resp[0])) {
            echo "\nEl vols ordenar per contingut ascendent o descendent? (1=ascendent, 2=descendent)\n";
            $filtrePer = Utils::llegirLinia();
            $check = Utils::checkTipus($filtrePer, [1, 2]);
            if ($check) {
                echo "\nOk";
                //Procedim
                $arrAfectatObj = $arrAfectatArr = array();
                if (is_array($obj->final)) {
                    foreach($obj->final as $item){
                        //excloem temporalment objects i arrays, ens porten problemes amb les funcions sort
                        $tipusItem = gettype($item);
                        if ($tipusItem=="object"){
                            $arrAfectatObj[]= $item;
                        }elseif ($tipusItem=="array"){
                            $arrAfectatArr[]= $item;
                        }else{ //string o integer
                            $arrAfectat[]= $item;
                        }
                    }
                }else{
                    echo "\nL'array està buit.";
                }
                if ($filtrePer == "1") {
                    $obj->historic[date("Y-m-d H:i:s")] = "Ordenar per contingut"; //asc
                    if (sort($arrAfectat,SORT_FLAG_CASE)) { //SORT_REGULAR, SORT_FLAG_CASE
                        if (is_array($arrAfectatArr) && count($arrAfectatArr)>0) {
                            foreach($arrAfectatArr as $item1){
                                array_push($arrAfectat, $item1); //al final
                            }
                        }
                        if (is_array($arrAfectatObj) && count($arrAfectatObj)>0) {
                            foreach($arrAfectatObj as $item1){
                                array_push($arrAfectat, $item1); //al final
                            }
                        }
                        $obj->final = $arrAfectat;
                        Utils::preguntaImprimir($obj);
                    } else {
                        echo "\nError. No s'ha pogut ordenar l'array.";
                    }
                } else {
                    $obj->historic[date("Y-m-d H:i:s")] = "Ordenar per contingut inversament"; //desc
                    if (rsort($arrAfectat,SORT_FLAG_CASE)) {
                        if (is_array($arrAfectatObj) && count($arrAfectatObj)>0) {
                            foreach($arrAfectatObj as $item1){
                                array_unshift($arrAfectat, $item1); //al inici
                            }
                        }
                        if (is_array($arrAfectatArr) && count($arrAfectatArr)>0) {
                            foreach($arrAfectatArr as $item1){
                                array_unshift($arrAfectat, $item1); //al inici
                            }
                        }
                        $obj->final = $arrAfectat;
                        Utils::preguntaImprimir($obj);
                    } else {
                        echo "\nError. No s'ha pogut ordenar inversament l'array.";
                    }
                }
            } else {
                echo "\nLa opció '$filtrePer' no està dins els paràmetres vàlids: [1,2] ";
            }
        }
        return $obj;
    }

    /**
     * @description Opció 2. Ordenar els tipus
     * @param Db $obj
     * @return Db
     */
    public static function ordenarPerTipus(Db $obj)
    {
        $resp = Utils::detectarTipus($obj);
        if (is_array($resp[0])) {
            echo "\nEl vols ordenar per tipus ascendent o descendent? (1=ascendent, 2=descendent)\n";
            $filtrePer = Utils::llegirLinia();
            $check = Utils::checkTipus($filtrePer, [1, 2]);
            if ($check) {
                echo "\nOk";
                //Procedim
                if ($filtrePer == "1") {
                    $obj->historic[date("Y-m-d H:i:s")] = "Ordenar per tipus";
                    if (sort($resp[0], SORT_REGULAR)) {
                        self::ordenarPerTipusFinal($obj, $resp[0]);
                    } else {
                        echo "\nError. No s'ha pogut ordenar l'array.";
                    }
                } else {
                    $obj->historic[date("Y-m-d H:i:s")] = "Ordenar per tipus inversament";
                    if (rsort($resp[0], SORT_REGULAR)) {
                        self::ordenarPerTipusFinal($obj, $resp[0]);
                    } else {
                        echo "\nError. No s'ha pogut ordenar inversament l'array.";
                    }
                }
            } else {
                echo "\nLa opció '$filtrePer' no està dins els paràmetres vàlids: [1,2] ";
            }
        }
        return $obj;
    }

    /**
     * @description Opció 2b. Ordenar per tipus amb els tipus ja ordenats
     * @param Db $obj
     * @param array $arrTipus
     * @return Db
     */
    public static function ordenarPerTipusFinal(Db $obj, $arrTipus)
    {
        $resp = Utils::detectarTipus($obj);
        if (is_array($arrTipus)) {
            if (is_array($obj->final)) {
                if (count($obj->final) > 0) {
                    foreach ($arrTipus as $tipusActual) {
                        foreach ($obj->final as $key => $item) {
                            $tipusItem = gettype($item);
                            if ($tipusActual == $tipusItem) {
                                $arrayAfectat[] = $item;
                            }
                        }
                    }
                } else {
                    echo "\nL'array actual està buit.";
                }
                $obj->final = $arrayAfectat;
                Utils::preguntaImprimir($obj);
            } else {
                echo "\nL'array proporcionat no es cap array.";
            }
        }
        return $obj;
    }

    /**
     * @description Opció 3. Demana el tipus per filtrar.
     * @param Db $obj
     * @return Db
     */
    public static function filtrarPerTipus(Db $obj)
    {
        $resp = Utils::detectarTipus($obj);
        if (is_array($resp[0])) {
            echo "\nPer quin tipus vols filtrar: [" . implode(',', $resp[0]) . "] ?\n";
            $filtrePer = Utils::llegirLinia();
            $check = Utils::checkTipus($filtrePer, $resp[0]);
            if ($check) {
                echo "\nOk";
                echo "\nVols veure únicament els de tipus '$filtrePer' o els vols excloure de l'array actual? (1=veure únicament, 2=exclou)\n";
                $inclosExclos = Utils::llegirLinia();
                $check = Utils::checkTipus($inclosExclos, [1, 2]);
                if ($check) {
                    echo "\nOk";
                    //Procedim
                    if ($inclosExclos == "1") {
                        $obj->historic[date("Y-m-d H:i:s")] = "Filtrar per tipus";
                        $obj = self::filtrarPerTipusFinal($obj, $filtrePer, "incloure"); //Unicament el tipus $filtrePer
                    } else {
                        $obj->historic[date("Y-m-d H:i:s")] = "Filtrar per tipus inversament";
                        $obj = self::filtrarPerTipusFinal($obj, $filtrePer, "excloure"); //Tots els tipus menys el tipus $filtrePer
                    }
                } else {
                    echo "\nLa opció '$inclosExclos' no està dins els paràmetres vàlids: [1,2] ";
                }
            } else {
                echo "\nLa opció '$filtrePer' no està dins els paràmetres vàlids: [" . implode(',', $resp[0]) . "] ";
            }
        }
        return $obj;
    }

    /**
     * @description Opció 3. Executa l'ordre
     * @param Db $obj
     * @param string $filtrePer
     * @param string $ordre
     * @return Db
     */
    public static function filtrarPerTipusFinal(Db $obj, $filtrePer, $ordre)
    {
        if (is_array($obj->final)) {
            if (count($obj->final) > 0) {
                foreach ($obj->final as $key => $item) {
                    $tipus = gettype($item);
                    //if (is_array($item) && count($item) > 0) {
                    //    $itemRandom = self::filtrarPerTipusFinalFill($item, $filtrePer, $ordre);
                    //    if (isset($itemRandom)) { //No sempre tindrà valor si l'array es mixte
                    //        $arrayAfectat[$key] = $itemRandom;
                    //    }
                    //} else {
                    if ($filtrePer == $tipus && $ordre == "excloure") {
                        //L'excloem
                    } elseif ($filtrePer != $tipus && $ordre == "excloure") {
                        $arrayAfectat[$key] = $item;
                    } elseif ($filtrePer != $tipus && $ordre == "incloure") {
                        //L'excloem
                    } elseif ($filtrePer == $tipus && $ordre == "incloure") {
                        $arrayAfectat[$key] = $item;
                    }
                    //}
                    //echo "\n".__METHOD__.__LINE__.": tipus:$tipus, \$ordre=$ordre, \$arrayAfectat[$key]=".$arrayAfectat[$key];
                }
            } else {
                echo "\nL'array actual està buit.";
            }
            if (isset($arrayAfectat)) { //No sempre tindrà valor si l'array es mixte
                $obj->final = $arrayAfectat;
            } else {
                $obj->final = array();
            }
            Utils::preguntaImprimir($obj);
        } else {
            echo "\nL'array proporcionat no es cap array.";
        }
        return $obj;
    }

    /**
     * @description Opció 3. Executa l'ordre pels fills de l'array. 
     * @nota Al final no l'executo perquè si es filtre per array, llavors queda un array buit (ja que els seus fills són de tipus string i els elimina)
     * @param array $arrayAfectatFill
     * @param string $filtrePer
     * @param string $ordre
     * @return ArrayOrString
     */
    public static function filtrarPerTipusFinalFill($arrayAfectatFill, $filtrePer, $ordre)
    {
        if (is_array($arrayAfectatFill)) {
            if (count($arrayAfectatFill) > 0) {
                foreach ($arrayAfectatFill as $key => $item) {
                    $tipus = gettype($item);
                    if (is_array($item) && count($item) > 0) {
                        $itemRandom = self::filtrarPerTipusFinalFill($item, $filtrePer, $ordre);
                        if (isset($itemRandom)) { //No sempre tindrà valor si l'array es mixte
                            $arrayAfectat[$key] = $itemRandom;
                        }
                    } else {
                        if ($filtrePer == $tipus && $ordre == "excloure") {
                            //L'excloem
                        } elseif ($filtrePer != $tipus && $ordre == "excloure") {
                            $arrayAfectat[$key] = $item;
                        } elseif ($filtrePer != $tipus && $ordre == "incloure") {
                            //L'excloem
                        } elseif ($filtrePer == $tipus && $ordre == "incloure") {
                            $arrayAfectat[$key] = $item;
                        }
                    }
                }
                if (isset($arrayAfectat)) { //No sempre tindrà valor si l'array es mixte
                    return $arrayAfectat;
                } else {
                    return array();
                }
            } else {
                return "Error: No té valors";
            }
        } else {
            return "Error: No es array";
        }
    }


    /**
     * @description Opció 4. Reordenar aleatòriament
     * @param Db $obj
     * @return Db
     */
    public static function reordenarRandom(Db $obj)
    {
        $obj->historic[date("Y-m-d H:i:s")] = "Reordenar aleatòriament";
        if (is_array($obj->final)) {
            //Abans del shuffle, mirar cada item si es un array, si ho es i no te mes arrays a dins, fer un shuffle
            if (count($obj->final) > 0) {
                foreach ($obj->final as $key => $item) {
                    if (is_array($item) && count($item) > 0) {
                        //echo "\n\t ".__METHOD__."::".__LINE__.": \$item Array: " . print_r($item, true);
                        $itemRandom = self::reordenarRandomFill($item);
                        //echo "\n\t ".__METHOD__."::".__LINE__.": \$itemRandom Array: " . print_r($itemRandom, true);
                        $arrayAfectat[$key] = $itemRandom;
                    } else {
                        $arrayAfectat[$key] = $item;
                    }
                }
            } else {
                echo "\nL'array actual està buit.";
            }
            shuffle($arrayAfectat);
            $obj->final = $arrayAfectat;
            Utils::preguntaImprimir($obj);
        } else {
            echo "\nL'array proporcionat no es cap array.";
        }
        return $obj;
    }

    /**
     * @description Opció 4.1. Reordenar aleatòriament
     * @param Array $arrayAfectatFill
     * @return ArrayOrString
     */
    public static function reordenarRandomFill($arrayAfectatFill)
    {
        if (is_array($arrayAfectatFill)) {
            //Abans del shuffle, mirar cada item si es un array, si ho és i no te mes arrays a dins, fer un shuffle
            if (count($arrayAfectatFill) > 0) {
                foreach ($arrayAfectatFill as $key => $item) {
                    if (is_array($item) && count($item) > 0) {
                        $itemRandom = self::reordenarRandomFill($item);
                        $arrayAfectatFill[$key] = $itemRandom;
                    } else {
                        $arrayAfectatFill[$key] = $item;
                    }
                }
                shuffle($arrayAfectatFill);
                return $arrayAfectatFill;
            } else {
                return "Error: No té valors";
            }
        } else {
            return "Error: No es array";
        }
    }

    /**
     * @description Opció 5. Resetejar el contingut a l'array original
     * @param Db $obj
     * @return Db
     */
    public static function resetArrayOriginal(Db $obj)
    {
        $obj->historic[date("Y-m-d H:i:s")] = "Resetejar el contingut a l'array original";
        $obj->final = $obj->origen;
        Utils::preguntaImprimir($obj);
        return $obj;
    }

    /**
     * @description Opció 6. Eliminar tots els elements que no continguin més de 3 caràcters entre 'n' i 't' (minúscules o majúscules)
     * @param Db $obj
     * @return Db
     */
    public static function eliminarElements3Chars(Db $obj)
    {
        if (is_array($obj->final)) {
            if (count($obj->final) > 0) {
                foreach ($obj->final as $key => $item) {
                    $tipus = gettype($item);
                    //1- ha de ser string
                    if ($tipus=="string"){
                        //2- Ha de tenir les lletres n i t (min/mayus)
                        $posN = stripos($item, "n");
                        $posT = stripos($item, "t");
                        //3- entre les lletres n i t han de tenir menys de 3 lletres
                        if ($posN!==false && $posT!==false){
                            $diffPos = abs($posT-$posN); //Ho posem en abs() perquè es podria complir la condició al revés, entre 't' i 'n'
                            if ($diffPos<=3){
                                $arrayAfectat[]=$item;
                            }
                        }
                    }
                }
            } else {
                echo "\nL'array actual està buit.";
            }
            if (isset($arrayAfectat)) { //No sempre tindrà valor 
                $obj->final = $arrayAfectat;
            } else {
                $obj->final = array();
            }
            Utils::preguntaImprimir($obj);
        } else {
            echo "\nL'array proporcionat no es cap array.";
        }
        return $obj;
    }

    /**
     * @description Opció 7. Imprimeix l'actual.
     * @param Db $obj
     * @return void
     */
    public static function imprimirActual(Db $obj)
    {
        $obj->historic[date("Y-m-d H:i:s")] = "Imprimir l'array actual";

        if (is_array($obj->final)) {
            if (count($obj->final) > 0) {
                echo "\nImprimint Actual:\n";
                var_dump($obj->final);
                /*foreach ($obj->final as $key => $item) {
                    $tipus = gettype($item);
                    if (is_array($item)) {
                        echo "\n$key => " . print_r($item, true);
                        //S'hauria pogut fer una funcio recursiva per fomatar millor $item si té més dimensions
                    } elseif (is_object($item)) {
                        echo "\n$key => " . print_r($item, true);
                        //En un cas similar al superior, es podria fer un foreach iterant totes les propietats del object
                        //Pero es podria complicar si l'object tingués una propietat amb un array o més objects, caldria controlar millor aquests casos
                    } else {
                        echo "\n$key => $item => $tipus";
                    }
                }*/
            } else {
                echo "\nNo hi ha contingut.";
            }
        } else {
            echo "\nNo hi ha actual per mostrar.";
        }
    }

    /**
     * @description Opció 8. Imprimeix l'origen.
     * @param Db $obj
     * @return void
     */
    public static function imprimirOrigen(Db $obj)
    {
        $obj->historic[date("Y-m-d H:i:s")] = "Imprimir l'array origen";

        if (is_array($obj->origen)) {
            if (count($obj->origen) > 0) {
                echo "\nImprimint Origen:\n";
                var_dump($obj->final);
                /*foreach ($obj->origen as $key => $item) {
                    $tipus = gettype($item);
                    if (is_array($item)) {
                        echo "\n$key => " . print_r($item, true);
                    } elseif (is_object($item)) {
                        echo "\n$key => " . print_r($item, true);
                    } else {
                        echo "\n$key => $item => $tipus";
                    }
                }*/
            } else {
                echo "\nNo hi ha contingut.";
            }
        } else {
            echo "\nNo hi ha origen per mostrar.";
        }
    }

    /**
     * @description Opció 9. Imprimeix l'històric.
     * @param Db $obj
     * @return void
     */
    public static function imprimirHistoric(Db $obj)
    {
        $obj->historic[date("Y-m-d H:i:s")] = "Imprimir l'historial de modificacions";

        if (is_array($obj->historic)) {
            if (count($obj->historic) > 0) {
                echo "\nImprimint Històric:";
                foreach ($obj->historic as $key => $item) {
                    echo "\n\t$key => $item";
                }
            } else {
                echo "\nNo hi ha cap canvi.";
            }
        } else {
            echo "\nNo hi ha històric per mostrar.";
        }
    }
}
