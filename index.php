<?php
require_once("libOrdenarArray/Controller.php");

$entorn = php_sapi_name();
echo "\n Entorn: $entorn";

echo "\n\n";
if ($entorn == 'cli') {
  //Entorn CLI
  /*
    echo "\n\n ".__LINE__.": argv:";
    var_dump($argv); //Global
    echo "\n\n ".__LINE__.": argc:";
    var_dump($argc); //Global
  */
  $obj = Controller::init();
  if ($argc >1 && isset($argv[1])){
    //echo "\n\n Parametre 1: ".$argv[1];
    Controller::route($obj, $argv);
  } else {
    echo "Crida l'arxiu amb el paràmetre '-h' per veure les opcions disponibles: ".$_SERVER["PHP_SELF"]." -h";
  }
} else {
  //Entorn Apache Nginx o similar
  echo "<h4>Cal executar aquest arxiu desde la línia de comandes.</h4>";
}
